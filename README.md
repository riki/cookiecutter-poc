# Cookiecutter POC
ref. https://github.com/cookiecutter/cookiecutter

## Requirements
Create a Python virtual environment with your tool of choice
and then install `cookiecutter`:
```bash
pip install cookiecutter
```

## Configuration
The whole project generation from template is based on a
single file: `cookiecutter.json`.

Make sure this file contains all the information needed to
specialize the template into a functional project.

## How it works
`cookiecutter` will look for all the instances of the entries
in `cookiecutter.json` in the current folder/subfolders
and replace them with the input value provided during
generation or with the default value.

## Usage
To generate a project from the template, simply run:
```bash
cookiecutter .
```

This command will generate a new folder whose content is
specialized based on the inputs provided during project
generation.

For more advanced usage, see `cookiecutter -h`.

## Example
`cookiecutter.json`:
```json
{
    "project_name": "hello",
    "project_slug": "{{ cookiecutter.project_name.lower().replace(' ', '-').replace('_', '-') }}"
}
```

Template structure:
```bash
cookiecutter-poc
├── README.md
├── cookiecutter.json
└── {{cookiecutter.project_slug}}
    ├── fixed.py
    └── {{cookiecutter.project_slug}}.sh
```

Generated project structure:
```bash
hello
├── fixed.py
└── hello.sh
```

Original file content (`fixed.py`):
```py
print("Hello from {{ cookiecutter.project_name }}!!")
```

Generated file content (`fixed.py`):
```py
print("Hello from hello!!")
```